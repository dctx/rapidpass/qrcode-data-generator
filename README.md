### About

Version 0.3

### Requirements

* Python 3.7+
* pipenv

### Usage

```
$ pipenv install
```

#### Generating a Vehicle QR

```
$ python vehicle.py
Serialized: b'\xaa\x0235YBRZ6E\x80\xf0\xbf\xe7\x0b\x80\x88\xea\xe7\x0b\x0eABC1234'
Signature: 8de061c3
$ hexdump -C vehicle.avro
00000000  aa 02 33 35 59 42 52 5a  36 45 80 f0 bf e7 0b 80  |..35YBRZ6E......|
00000010  88 ea e7 0b 0e 41 42 43  31 32 33 34              |.....ABC1234|
0000001c
$ hexdump -C vehicle.qrdata
00000000  aa 02 33 35 59 42 52 5a  36 45 80 f0 bf e7 0b 80  |..35YBRZ6E......|
00000010  88 ea e7 0b 0e 41 42 43  31 32 33 34 8d e0 61 c3  |.....ABC1234..a.|
```

#### Generating an Individual QR

```
$ python individual.py
Serialized: b'\xaa\x020C6BW6YP\x80\xf0\xbf\xe7\x0b\x80\x88\xea\xe7\x0b\x1aDN95012345678'
Signature: 8c828dfa
$ hexdump -C individual.avro
00000000  aa 02 30 43 36 42 57 36  59 50 80 f0 bf e7 0b 80  |..0C6BW6YP......|
00000010  88 ea e7 0b 1a 44 4e 39  35 30 31 32 33 34 35 36  |.....DN950123456|
00000020  37 38                                             |78|
00000022
$ hexdump -C individual.qrdata
00000000  aa 02 30 43 36 42 57 36  59 50 80 f0 bf e7 0b 80  |..0C6BW6YP......|
00000010  88 ea e7 0b 1a 44 4e 39  35 30 31 32 33 34 35 36  |.....DN950123456|
00000020  37 38 8c 82 8d fa                                 |78....|
00000026
```
