use image::Luma;
use qrcode::QrCode;
use std::env;
use std::fs;
use std::io::Write;

static USAGE: &[u8] = r#"
Usage:

  qr <input_file> <output_png>

Where:

  <input_file> - is the file containing the (binary) data to be encoded
  <output_png> - the name of the output PNG file containing the QR Code
"#
.as_bytes();

fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        std::io::stdout().write_all(USAGE)?;

        std::process::exit(1)
    } else {
        let input_file = &args[1];
        let output_png = &args[2];

        // Read the serialized data
        let data = &fs::read(input_file)?;

        // Encode some data into bits.
        let code = QrCode::new(data).unwrap();

        // Render the bits into an image.
        let image = code.render::<Luma<u8>>().build();

        // Save the image.
        image.save(output_png).unwrap();

        Ok(())
    }
}
