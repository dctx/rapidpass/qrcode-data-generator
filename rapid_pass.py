import io
import skippy
import base32_crockford
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, hmac, serialization
from cryptography.hazmat.primitives.asymmetric import padding
from damm32 import damm32

# Some constants for the pass type
T_INDIVIDUAL = 0b00000000
T_VEHICLE = 0b10000000
APOR_GOVERMENT = 'GO'


class RapidPass:
    pass_type = T_INDIVIDUAL
    apor = ''
    control_number = None
    valid_from = None
    valid_until = None
    id_or_plate = ''

    def __init__(self, pass_type=T_INDIVIDUAL):
        self.pass_type = pass_type

    @staticmethod
    def vehicle():
        return RapidPass(T_VEHICLE)

    @staticmethod
    def individual():
        return RapidPass(T_INDIVIDUAL)

    def serialize(self):
        with io.BytesIO() as buf:
            apor = self.apor
            apor_bytes = apor[0:2].encode()
            buf.write(byte(self.pass_type | apor_bytes[0]))
            buf.write(byte(apor_bytes[1]))
            buf.write(fixed32(self.control_number))
            buf.write(fixed32(self.valid_from))
            buf.write(fixed32(self.valid_until))
            buf.write(byte(len(self.id_or_plate)))
            buf.write(self.id_or_plate.encode())
            return buf.getvalue()

    def signature(self):
        return hmac_short_sign(self.serialize())

    def serialize_with_signature(self):
        serialized = self.serialize()
        sig = hmac_short_sign(serialized)
        return append_bytes(serialized, sig)


def type_and_purpose(i_or_v, purpose):
    byte = i_or_v | purpose
    return byte.to_bytes(1, byteorder="big")


def byte(b):
    return b.to_bytes(1, byteorder="big")


def fixed32(n):
    return n.to_bytes(4, byteorder="big")


CIPHER = skippy.Skippy(b'SKIP32_SECRET_KEY')


def skip32(n):
    return CIPHER.encrypt(n)


def base32(n):
    encoded = base32_crockford.encode(n)
    s = encoded.zfill(7)
    return s + base32_crockford.encode(damm32(n))


def rsa_private_key_load(filename):
    with open(filename, "rb") as key_file:
        return serialization.load_pem_private_key(
            key_file.read(),
            password=None,
            backend=default_backend()
        )


RSA_PRIVATE_KEY = rsa_private_key_load("keys/private")


def rsa_short_sign(data):
    signature = RSA_PRIVATE_KEY.sign(
        data,
        padding.PKCS1v15(),
        hashes.SHA256()
    )
    return signature[:4]


HMAC_KEY = bytes.fromhex(
    "a993cb123b3ba87bf06c22365bfa0951e2521fcff5990b42dacf7c4b55e83a42")


def hmac_short_sign(data):
    h = hmac.HMAC(HMAC_KEY, hashes.SHA256(), backend=default_backend())
    h.update(data)
    return h.finalize()[:4]


def append_bytes(a, b):
    buf = io.BytesIO()
    buf.write(a)
    buf.write(b)
    return buf.getvalue()


def write_to_file(filename, data):
    with open(filename, "wb") as f:
        f.write(data)
        f.flush()


def control_code_for(n):
    return base32(skip32(n))
