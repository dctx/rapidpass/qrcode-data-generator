import base32_crockford
import rapid_pass
import xlrd
from base64 import b64encode
from datetime import datetime
from rapid_pass import RapidPass

# Some constants for the pass type
T_INDIVIDUAL = 0b00000000
T_VEHICLE = 0b10000000


def xldate_to_timestamp(datemode, xld):
    return int(datetime.timestamp(xlrd.xldate_as_datetime(xld, datemode)))


def encode(datemode, row):
    v_or_i = row[0].value.lower()
    rp = RapidPass.vehicle() if v_or_i == 'vehicle' else RapidPass.individual()
    cc = row[1].value
    control_code = cc if len(cc) == 7 else cc[0:7]
    rp.control_number = base32_crockford.decode(control_code)
    rp.apor = row[2].value
    rp.valid_from = xldate_to_timestamp(datemode, row[3].value)
    rp.valid_until = xldate_to_timestamp(datemode, row[4].value)
    rp.id_or_plate = row[5].value
    qr = rp.serialize_with_signature()
    print('{} => {} (base64: {})'.format(
        control_code, qr.hex(), b64encode(qr).decode()))


def main():
    book = xlrd.open_workbook('Sample QR Codes.xlsx')
    sheet = book.sheet_by_index(0)
    nrows = sheet.nrows
    for r in range(1, nrows):
        encode(book.datemode, sheet.row(r))


if __name__ == "__main__":
    main()
