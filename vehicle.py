import io
import json
import base64
import rapid_pass
from datetime import datetime
from rapid_pass import RapidPass

# Some constants for the pass type
T_INDIVIDUAL = 0b00000000
T_VEHICLE = 0b10000000
APOR_GOVERNMENT = 'GO'

# RapidPass Data
PASS_ID = 1234
PASS_ID_SKIP_32 = rapid_pass.skip32(PASS_ID)
VALID_FROM = int(datetime.timestamp(datetime(2020, 3, 23, 8, 0, 0)))
VALID_UNTIL = int(datetime.timestamp(datetime(2020, 3, 27, 8, 0, 0)))
ID_OR_PLATE = 'ABC1234'


def main():
    rapidpass = RapidPass.vehicle()
    rapidpass.apor = APOR_GOVERNMENT
    rapidpass.control_number = PASS_ID_SKIP_32
    rapidpass.valid_from = VALID_FROM
    rapidpass.valid_until = VALID_UNTIL
    rapidpass.id_or_plate = ID_OR_PLATE

    serialized = rapidpass.serialize()
    print("Custom: {} ({} bytes)".format(serialized.hex(), len(serialized)))
    rapid_pass.write_to_file("vehicle.bin", serialized)

    sig = rapidpass.signature()
    print("Signature: {}".format(sig.hex()))

    payload = rapidpass.serialize_with_signature()
    rapid_pass.write_to_file("vehicle.qrdata", payload)

    base64_encoded = base64.b64encode(payload).decode()
    print("Base64: {}".format(base64_encoded))


if __name__ == "__main__":
    main()
