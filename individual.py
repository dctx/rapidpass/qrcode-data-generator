import io
import json
import skippy
import base32_crockford
import base64
import rapid_pass
from base64 import b64encode
from datetime import datetime
from fastavro import parse_schema, schemaless_writer
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from damm32 import damm32
from rapid_pass import RapidPass

# Some constants for the pass type
T_INDIVIDUAL = 0b00000000
T_VEHICLE = 0b10000000
APOR_GOVERNMENT = 'GO'

# RapidPass Data
PASS_ID = 98765
PASS_ID_SKIP_32 = rapid_pass.skip32(PASS_ID)
VALID_FROM = int(datetime.timestamp(datetime(2020, 3, 23, 8, 0, 0)))
VALID_UNTIL = int(datetime.timestamp(datetime(2020, 3, 27, 8, 0, 0)))
ID_OR_PLATE = "N95012345678"


def main():
    rapidpass = RapidPass.individual()
    rapidpass.apor = APOR_GOVERNMENT
    rapidpass.control_number = PASS_ID_SKIP_32
    rapidpass.valid_from = VALID_FROM
    rapidpass.valid_until = VALID_UNTIL
    rapidpass.id_or_plate = ID_OR_PLATE

    serialized = rapidpass.serialize()
    print("Custom: {} ({} bytes)".format(serialized.hex(), len(serialized)))
    rapid_pass.write_to_file("individual.bin", serialized)

    sig = rapidpass.signature()
    print("Signature: {}".format(sig.hex()))

    payload = rapidpass.serialize_with_signature()
    rapid_pass.write_to_file("individual.qrdata", payload)

    base64_encoded = base64.b64encode(payload).decode()
    print("Base64: {}".format(base64_encoded))


if __name__ == "__main__":
    main()
